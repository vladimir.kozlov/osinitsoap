CREATE DATABASE osinitsoap
    WITH ENCODING = 'UTF-8'
    OWNER = postgres
    CONNECTION LIMIT = 25;

CREATE TABLE usr (
    id INT PRIMARY KEY NOT NULL,
    username VARCHAR (50) NOT NULL
);

INSERT INTO usr(id, username) VALUES (1, 'First User');
INSERT INTO usr(id, username) VALUES (2, 'Second User');
INSERT INTO usr(id, username) VALUES (3, 'Third User');
INSERT INTO usr(id, username) VALUES (4, 'Fourth User');
INSERT INTO usr(id, username) VALUES (5, 'Fifth User');