package com.osinitsoap.service;

import com.osinitsoap.dao.UserDao;

public class UserService {
    private static final String NOT_FOUND_MESSAGE = "User not found";
    private static final UserDao userDao = new UserDao();

    public static String getUserName(int userId) {
        String userName = userDao.getUser(userId);

        return userName != null ? userName : NOT_FOUND_MESSAGE;
    }
}
