package com.osinitsoap.dao;

import java.sql.*;
import java.util.Properties;

public class UserDao {
    private String url = null;
    private String username = null;
    private String password = null;
    private String sqlQuery = null;

    public UserDao() {
        try {
            Properties properties = new Properties();
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("db.properties"));
            url = properties.getProperty("jdbc.url");
            username = properties.getProperty("jdbc.username");
            password = properties.getProperty("jdbc.password");
            sqlQuery = properties.getProperty("sql.query");
        } catch (Exception e) {
            System.err.format("Error loading JDBS params from props'file");
        }
    }

    public String getUser(int userId) {
        String result = null;

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.err.format("Error loading PostgreSQL JDBC Driver");
        }

        try {
            conn = DriverManager.getConnection(url, username, password);

            preparedStatement = conn.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, userId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getString("username");
            }
        } catch (SQLException e) {
            System.err.format("\nSQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (preparedStatement != null) {
                    preparedStatement.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.err.format(e.getMessage());
            }
        }

        return result;
    }
}