package com.osinitsoap.route;

import com.osinitsoap.GetUserRequest;
import com.osinitsoap.GetUserResponse;
import com.osinitsoap.service.UserService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

public class UserRoute extends RouteBuilder{

    @Override
    public void configure() throws Exception {

        from("cxf:bean:cxfSoapServiceEndpoint")
                .process(new Processor() {

                    public void process(Exchange exchange) throws Exception {

                        final GetUserRequest getUserRequest = exchange.getIn().getBody(GetUserRequest.class);

                        final GetUserResponse userResponse = new GetUserResponse();
                        userResponse.setOutputUserName(UserService.getUserName(getUserRequest.getInputUserId()));

                        exchange.getOut().setBody(userResponse);
                    }
                });
    }
}
